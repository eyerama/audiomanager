//
//  ViewController.m
//  AudioManagerSample
//
//  Created by eyemac on 2014. 5. 14..
//  Copyright (c) 2014년 onsetel. All rights reserved.
//

#import "ViewController.h"
#import "AudioManager.h"
#import "AMOscilloscopeView.h"

@interface ViewController ()<AudioRecorderDelegate, AudioPlayerDelegate>
@property (retain, nonatomic) IBOutlet UILabel *stateLabel;
@property (retain, nonatomic) IBOutlet UIButton *recordButton;
@property (retain, nonatomic) IBOutlet UIButton *playButton;
@property (retain, nonatomic) IBOutlet UIButton *speakerButton;
@property (retain, nonatomic) IBOutlet AMOscilloscopeView *oscilloscopeView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self.recordButton setTitle:@"RECORD"
                       forState:UIControlStateNormal];
    [self.recordButton sizeToFit];
    
    [AudioManager sharedManager].recordDelegate = self;
    [AudioManager sharedManager].playerDelegate = self;
    [AudioManager sharedManager].passTruDelegate = self.oscilloscopeView;
//    [[AudioManager sharedManager] prepareRecord:^(BOOL success) {
//        if (success) {
//            [self.stateLabel setText:@"Recorder prepare 성공~"];
//        }
//        else
//        {
//            [self.stateLabel setText:@"Recorder prepare 실패~"];
//        }
//    }];
}
- (IBAction)startRecord:(id)sender {
    if ([AudioManager sharedManager].isRecording) {
        [[AudioManager sharedManager] stopRecord];
    }
    else
    {
        [[AudioManager sharedManager] prepareAudioSession:^(BOOL success) {
            if (success) {
                [[AudioManager sharedManager] startRecord:NO];
            }
        }];
    }
}

- (IBAction)startPlay:(id)sender
{
    if ([AudioManager sharedManager].isPlaying) {
        [[AudioManager sharedManager] stopPlay];
    }
    else
    {
        [[AudioManager sharedManager] prepareAudioSession:^(BOOL success) {
            if (success) {
                
                NSString *docDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                             NSUserDomainMask,
                                                                             YES).firstObject;
                NSString *audioFilePath = docDirectory;
                for (NSString *filename in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:docDirectory
                                                                                               error:nil]) {
                    audioFilePath = [docDirectory stringByAppendingPathComponent:filename];
                }
                [[AudioManager sharedManager] startPlayWithFile:[NSURL fileURLWithPath:audioFilePath]];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_stateLabel release];
    [_recordButton release];
    [_playButton release];
    [_speakerButton release];
    [_oscilloscopeView release];
    [super dealloc];
}

#pragma mark - AudioRecorderDelegate

- (void)audioRecord:(AudioManager *)recorder
          peakLevel:(CGFloat)peakLevel
{
    
}

- (void)audioRecord:(AudioManager *)recorder
        processTime:(NSTimeInterval)processTime
{
    NSInteger minutes;
    NSInteger seconds;
    [AudioManager ConvertProcessTime:processTime
                             seconds:&seconds
                             minutes:&minutes
                               hours:NULL];
    self.title = [NSString stringWithFormat:@"%2.2d:%2.2d", minutes, seconds];
}

- (void)audioRecordFailed:(AudioManager *)recorder error:(NSError *)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"AudioManager"
                                                        message:[NSString stringWithFormat:@"Error\n%@", error.userInfo[NSLocalizedFailureReasonErrorKey]]
                                                       delegate:nil
                                              cancelButtonTitle:@"확인"
                                              otherButtonTitles:nil];
    [alertView show];
    [alertView release];
}

- (void)audioRecordFinished:(AudioManager *)recorder audioData:(NSData *)audioData
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self.recordButton setTitle:@"RECORD"
                       forState:UIControlStateNormal];
    [self.recordButton sizeToFit];
    
    [recorder stopPassTruToSpeakerFromMic];
    [self.oscilloscopeView stop];
}

- (void)audioRecordStart:(AudioManager *)recorder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self.recordButton setTitle:@"STOP"
                       forState:UIControlStateNormal];
    [self.recordButton sizeToFit];
    
    [recorder requestPassTruToSpeakerFromMic];
}

#pragma mark - AudioPlayerDelegate

- (void)audioPlayStart:(AudioManager *)player
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self.playButton setTitle:@"STOP PLAY"
                       forState:UIControlStateNormal];
    [self.playButton sizeToFit];
}
- (void)audioPlayFinished:(AudioManager *)player
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self.playButton setTitle:@"PLAY"
                       forState:UIControlStateNormal];
    [self.playButton sizeToFit];
    [self.oscilloscopeView stop];
}
- (void)audioPlayFailed:(AudioManager *)player
                  error:(NSError *)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"AudioPlayer"
                                                        message:[NSString stringWithFormat:@"Error\n%@", error.userInfo[NSLocalizedFailureReasonErrorKey]]
                                                       delegate:nil
                                              cancelButtonTitle:@"확인"
                                              otherButtonTitles:nil];
    [alertView show];
    [alertView release];
}
- (void)audioPlay:(AudioManager *)player
      processTime:(NSTimeInterval)processTime
{
    NSInteger minutes;
    NSInteger seconds;
    [AudioManager ConvertProcessTime:processTime
                             seconds:&seconds
                             minutes:&minutes
                               hours:NULL];
    self.title = [NSString stringWithFormat:@"%2.2d:%2.2d", minutes, seconds];
}
- (void)audioPlay:(AudioManager *)player
        peakLevel:(CGFloat)peakLevel
{
    NSLog(@"%s : %f", __PRETTY_FUNCTION__, peakLevel);
}
- (IBAction)pressedSpeakerButton:(id)sender {
    if ([[AudioManager sharedManager] isInPassTru]) {
        [[AudioManager sharedManager] stopPassTruToSpeakerFromMic];
        [self.oscilloscopeView stop];
    }
    else
    {
    [[AudioManager sharedManager] prepareAudioSession:^(BOOL success) {
        if (success) {
            [[AudioManager sharedManager] requestPassTruToSpeakerFromMic];
            [self.oscilloscopeView start];
        }
    }];
    }
}

@end
