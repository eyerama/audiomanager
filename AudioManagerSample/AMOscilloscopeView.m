//
//  AMOscilloscopeView.m
//  AudioManagerSample
//
//  Created by eyemac on 2014. 5. 16..
//  Copyright (c) 2014년 onsetel. All rights reserved.
//

#import "AMOscilloscopeView.h"
#import <QuartzCore/QuartzCore.h>
#import <libkern/OSAtomic.h>
#import <Accelerate/Accelerate.h>

@interface AMOscilloscopeViewLayer ()<AudioPassTruDelegate>


@end

@implementation AMOscilloscopeViewLayer

- (void)start
{
    self.contentsScale = [UIScreen mainScreen].scale;
    self.actions = [NSDictionary dictionaryWithObject:[NSNull null] forKey:@"contents"];
    if ( _timer ) return;
    _ringBuffer = (float*)calloc(kRingBufferLength, sizeof(float));
    _scratchBuffer = (float*)malloc(kRingBufferLength * sizeof(float) * 2);
    if ( NSClassFromString(@"CADisplayLink") ) {
        _timer = [CADisplayLink displayLinkWithTarget:self selector:@selector(setNeedsDisplay)];
        ((CADisplayLink*)_timer).frameInterval = 0;
        [_timer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    } else {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0/30.0 target:self selector:@selector(setNeedsLayout) userInfo:nil repeats:YES];
    }
}

- (void)stop {
    if ( !_timer ) return;
    [_timer invalidate];
    _timer = nil;
    
    if (toFloatConverter) {
        AudioConverterDispose(toFloatConverter);
        toFloatConverter = NULL;
    }
}

- (void)drawInContext:(CGContextRef)ctx
{
    CGContextSetLineWidth(ctx, 2);
    CGContextSetStrokeColorWithColor(ctx, [_lineColor CGColor]);
    
    float multiplier = self.bounds.size.height;
    float midpoint = self.bounds.size.height / 2.0;
    float xIncrement = self.bounds.size.width / kRingBufferLength;
    
    // Render in contiguous segments, wrapping around if necessary
    int remainingFrames = kRingBufferLength-1;
    int tail = (_ringBufferHead+1) % kRingBufferLength;
    float x = 0;
    
    CGContextBeginPath(ctx);
    
    while ( remainingFrames > 0 ) {
        int framesToRender = MIN(remainingFrames, kRingBufferLength - tail);
        
        vDSP_vramp(&x, &xIncrement, _scratchBuffer, 2, framesToRender);
        vDSP_vsmul(&_ringBuffer[tail], 1, &multiplier, _scratchBuffer+1, 2, framesToRender);
        vDSP_vsadd(_scratchBuffer+1, 2, &midpoint, _scratchBuffer+1, 2, framesToRender);
        
        CGContextAddLines(ctx, (CGPoint*)_scratchBuffer, framesToRender);
        
        x += (framesToRender-1)*xIncrement;
        tail += framesToRender;
        if ( tail == kRingBufferLength ) tail = 0;
        remainingFrames -= framesToRender;
    }
    
    CGContextStrokePath(ctx);
}

- (void)audioPassTruStart:(AudioManager *)am
                     asbd:(AudioStreamBasicDescription)asbd
{
    srcStreamBasicDescription = asbd;

    floatingStreamBasicDescription.mFormatID          = kAudioFormatLinearPCM;
    floatingStreamBasicDescription.mFormatFlags       = kAudioFormatFlagIsFloat | kAudioFormatFlagIsPacked | kAudioFormatFlagIsNonInterleaved;
    floatingStreamBasicDescription.mChannelsPerFrame  = srcStreamBasicDescription.mChannelsPerFrame;
    floatingStreamBasicDescription.mBytesPerPacket    = sizeof(float);
    floatingStreamBasicDescription.mFramesPerPacket   = 1;
    floatingStreamBasicDescription.mBytesPerFrame     = sizeof(float);
    floatingStreamBasicDescription.mBitsPerChannel    = 8 * sizeof(float);
    floatingStreamBasicDescription.mSampleRate        = srcStreamBasicDescription.mSampleRate;
    
    OSStatus err = AudioConverterNew(&srcStreamBasicDescription,
                                     &floatingStreamBasicDescription,
                                     &toFloatConverter);
    
    NSLog(@"%@", UInt32ToString(err));
    
    AudioBufferList *convertBufferList = [AudioManager AMAllocateBufferList:floatingStreamBasicDescription
                                                                 frameCount:kMaxConversionSize];
    floatingBufferList = convertBufferList;
    
}

#define kNoMoreDataErr -2222

struct complexInputDataProc_t {
    AudioBufferList *sourceBuffer;
};

static OSStatus complexInputDataProc(AudioConverterRef             inAudioConverter,
                                     UInt32                        *ioNumberDataPackets,
                                     AudioBufferList               *ioData,
                                     AudioStreamPacketDescription  **outDataPacketDescription,
                                     void                          *inUserData) {
    struct complexInputDataProc_t *arg = (struct complexInputDataProc_t*)inUserData;
    if ( !arg->sourceBuffer ) {
        return kNoMoreDataErr;
    }
    
    memcpy(ioData, arg->sourceBuffer, sizeof(AudioBufferList) + (arg->sourceBuffer->mNumberBuffers-1)*sizeof(AudioBuffer));
    arg->sourceBuffer = NULL;
    
    return noErr;
}

- (void)audioPassTru:(AudioManager *)am
              frames:(UInt32)frames
          bufferList:(AudioBufferList *)bufferList
{
    // Convert
    if (nil == floatingBufferList) {
        return;
    }
//    AEFloatConverterToFloatBufferList(self.floatConverter, bufferList, floatingBufferList, frames);
    assert(floatingBufferList->mNumberBuffers == floatingStreamBasicDescription.mChannelsPerFrame);
    
    float *targetBuffers[floatingBufferList->mNumberBuffers];
    for ( int i=0; i<floatingBufferList->mNumberBuffers; i++ ) {
        targetBuffers[i] = (float*)floatingBufferList->mBuffers[i].mData;
    }
    
    if ( frames != 0 )
    {
        if ( toFloatConverter ) {
            UInt32 priorDataByteSize = bufferList->mBuffers[0].mDataByteSize;
            for ( int i=0; i<bufferList->mNumberBuffers; i++ ) {
                bufferList->mBuffers[i].mDataByteSize = frames * srcStreamBasicDescription.mBytesPerFrame;
            }
            
            for ( int i=0; i < floatingBufferList->mNumberBuffers; i++ ) {
                floatingBufferList->mBuffers[i].mData = targetBuffers[i];
                floatingBufferList->mBuffers[i].mDataByteSize = frames * sizeof(float);
            }
            
            OSStatus result = AudioConverterFillComplexBuffer(toFloatConverter,
                                                              complexInputDataProc,
                                                              &(struct complexInputDataProc_t) { .sourceBuffer = bufferList },
                                                              &frames,
                                                              floatingBufferList,
                                                              NULL);
            if (noErr != result) {
                NSLog(@"%@", UInt32ToString(result));
            }
            for ( int i=0; i<bufferList->mNumberBuffers; i++ ) {
                bufferList->mBuffers[i].mDataByteSize = priorDataByteSize;
            }
            
        }
    
    }
    // Get a pointer to the audio buffer that we can advance
    float *audioPtr = floatingBufferList->mBuffers[0].mData;
    
    // Copy in contiguous segments, wrapping around if necessary
    int remainingFrames = frames;
    while ( remainingFrames > 0 ) {
        int framesToCopy = MIN(remainingFrames, kRingBufferLength - self->_ringBufferHead);
        
        memcpy(_ringBuffer + _ringBufferHead, audioPtr, framesToCopy * sizeof(float));
        audioPtr += framesToCopy;
        
        int buffer_head = _ringBufferHead + framesToCopy;
        if ( buffer_head == kRingBufferLength ) buffer_head = 0;
        OSMemoryBarrier();
        _ringBufferHead = buffer_head;
        remainingFrames -= framesToCopy;
    }
}

@end

@interface AMOscilloscopeView ()<AudioPassTruDelegate>

@end

@implementation AMOscilloscopeView

+ (Class)layerClass
{
    return [AMOscilloscopeViewLayer class];
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.layer.frame = frame;
}

- (void)audioPassTruStart:(AudioManager *)am
                     asbd:(AudioStreamBasicDescription)asbd
{
    [(AMOscilloscopeViewLayer *)self.layer audioPassTruStart:am
                                                        asbd:asbd];
    [self start];
}

- (void)start
{
    [(AMOscilloscopeViewLayer *)self.layer start];
}

- (void)stop
{
    [(AMOscilloscopeViewLayer *)self.layer stop];
}

#pragma mark - AudioPassTruDelegate

- (void)audioPassTru:(AudioManager *)am
              frames:(UInt32)frames
          bufferList:(AudioBufferList *)bufferList
{
    [(AMOscilloscopeViewLayer *)self.layer audioPassTru:am
                                                 frames:frames
                                             bufferList:bufferList];
}

@end
