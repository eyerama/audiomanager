//
//  AMOscilloscopeView.h
//  AudioManagerSample
//
//  Created by eyemac on 2014. 5. 16..
//  Copyright (c) 2014년 onsetel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioManager.h"

#define kRingBufferLength 256
#define kMaxConversionSize 4096

@interface AMOscilloscopeViewLayer : CALayer
{
    id      _timer;
    float     *_scratchBuffer;
    float      *_ringBuffer;
    int     _ringBufferHead;
    
    AudioConverterRef toFloatConverter;
    
    AudioStreamBasicDescription srcStreamBasicDescription;
    AudioStreamBasicDescription floatingStreamBasicDescription;
    AudioBufferList *floatingBufferList;
}
@property (nonatomic, assign) UIColor *lineColor;

- (void)start;
- (void)stop;

@end

@interface AMOscilloscopeView : UIView<AudioPassTruDelegate>

- (void)start;
- (void)stop;

@end
