//
//  AppDelegate.h
//  AudioManagerSample
//
//  Created by eyemac on 2014. 5. 14..
//  Copyright (c) 2014년 onsetel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
