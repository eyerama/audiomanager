//
//  AudioCapture.h
//  GlobalFriend
//
//  Created by eyemac on 2014. 5. 13..
//  Copyright (c) 2014년 OnseTel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>

@interface NSString (UInt32ToString)

+ (NSString *)UInt32ToString:(UInt32)uint32;

@end

#define UInt32ToString(uint32) [NSString UInt32ToString:uint32]
#define CheckError(uint32, desc) [Au

typedef NS_ENUM(NSInteger, AudioCallbackFrom) {
    AudioCallbackFromPlay,
    AudioCallbackFromRecord,
    AudioCallbackFromPassTru,
};

@protocol AudioRecorderDelegate;
@protocol AudioPlayerDelegate;
@protocol AudioPassTruDelegate;

extern int const kNumberBuffers;
extern CGFloat const kBufferDuration;
extern NSString *const kAudioFileName;

@interface AudioManager : NSObject

@property (nonatomic, assign) id<AudioRecorderDelegate> recordDelegate;
@property (nonatomic, assign) id<AudioPlayerDelegate> playerDelegate;
@property (nonatomic, assign) id<AudioPassTruDelegate> passTruDelegate;

@property (nonatomic, assign) CGFloat timeGap;
@property (nonatomic, assign, getter = isRecording) BOOL recording;
@property (nonatomic, assign, getter = isPaused) BOOL paused;
@property (nonatomic, assign, getter = isPlaying) BOOL playing;
@property (nonatomic, assign) CGFloat recordDuration;

@property (nonatomic, assign) AudioFileTypeID   audioFileType;
@property (nonatomic, assign) UInt32            audioEncodingFormat;
@property (nonatomic, readonly) NSString        *fileExtension;

@property (nonatomic, readonly, getter = isInPassTru) BOOL inPassTru;

+ (AudioManager *)sharedManager;

+ (BOOL)IsAvailableAudioFileTypeAndFormat:(AudioFileTypeAndFormatID)fileTypeAndFormatID;
+ (NSString *)AudioFileExtension:(AudioFileTypeID)audioFileType;

+ (void)ConvertProcessTime:(NSTimeInterval)processTime
                   seconds:(NSInteger *)seconds
                   minutes:(NSInteger *)minutes
                     hours:(NSInteger *)hours;

+ (AudioBufferList *)AMAllocateBufferList:(AudioStreamBasicDescription)asbd
                  frameCount:(int)frameCount;

- (void)CheckError:(OSStatus)err
    errDescription:(NSString *)description
           succeed:(void (^)(BOOL success))succeed;

- (void)prepareAudioSession:(void (^)(BOOL success))prepare;

- (Float32)currentLevelMeter;

- (void)requestPassTruToSpeakerFromMic;
- (void)stopPassTruToSpeakerFromMic;

/**
 *  녹음 시작 메소드
 *
 *  @param paused pause 상태에서 다시 녹음 하는 경우 YES 전달 그 외에는 NO
 */
- (void)startRecord:(BOOL)paused;
- (void)pauseRecord;
- (void)stopRecord;

/**
 *  플레이 시작 메소드
 */
- (void)startPlayWithFile:(NSURL *)fileURL;
- (void)startPlay:(BOOL)paused;
- (void)pausePlay;
- (void)stopPlay;

- (void)saveDataToAudioFile:(NSURL *)fileURL
                       data:(NSData *)data
audioStreamBasicDescription:(AudioStreamBasicDescription)asbd;

- (void)generateSampleSound;

@end

@protocol AudioRecorderDelegate <NSObject>

- (void)audioRecordStart:(AudioManager *)recorder;
- (void)audioRecordFinished:(AudioManager *)recorder
                  audioData:(NSData *)audioData;
- (void)audioRecord:(AudioManager *)recorder
          processTime:(NSTimeInterval)processTime;
- (void)audioRecord:(AudioManager *)recorder
          peakLevel:(CGFloat)peakLevel;

- (void)audioRecordFailed:(AudioManager *)recorder
                    error:(NSError *)error;

@end

@protocol AudioPlayerDelegate <NSObject>

- (void)audioPlayStart:(AudioManager *)player;
- (void)audioPlayFinished:(AudioManager *)player;
- (void)audioPlayFailed:(AudioManager *)player
                  error:(NSError *)error;
- (void)audioPlay:(AudioManager *)player
      processTime:(NSTimeInterval)processTime;
- (void)audioPlay:(AudioManager *)player
        peakLevel:(CGFloat)peakLevel;

@end

@protocol AudioPassTruDelegate <NSObject>

- (void)audioPassTruStart:(AudioManager *)am
                     asbd:(AudioStreamBasicDescription)asbd;
- (void)audioPassTru:(AudioManager *)am
              frames:(UInt32)frames
          bufferList:(AudioBufferList *)bufferList;

@end