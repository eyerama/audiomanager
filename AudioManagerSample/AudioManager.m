//
//  AudioCapture.m
//  GlobalFriend
//
//  Created by eyemac on 2014. 5. 13..
//  Copyright (c) 2014년 OnseTel. All rights reserved.
//

#import "AudioManager.h"

@implementation NSString (UInt32ToString)

+ (NSString *)UInt32ToString:(UInt32)uint32
{
    UInt32 code = CFSwapInt32HostToBig(uint32);
    BOOL isString = YES;
    for (int i= 0; i < 4; i++) {
        char c = (char)(((char *)&code)[i]);
        if (false == isprint(c)){
            isString = NO;
            break;
        }
    }
    
    if (isString) {
        return [NSString stringWithFormat:@"%4.4s", (char *)&code];
    }
    else
    {
        return [NSString stringWithFormat:@"%d", (int)uint32];
    }
    return @"";
}

@end

int const kNumberBuffers = 3;
CGFloat const kBufferDuration = 0.5;
NSString *const kAudioFileName = @"record_template";

@interface AudioManager ()
{
    AudioQueueBufferRef buffers[kNumberBuffers];
    
    SInt64 playPacketPosition;
    UInt32 playNumPacketsToRead;
    AudioStreamPacketDescription *playAudioPacketDescriptions;
    BOOL isDone;
    
    AudioUnit _remoteAudioUnit;
}
@property (nonatomic, assign) BOOL audioInputAvailable;
@property (nonatomic, copy) void (^prepareBlock)(BOOL success);

@property (nonatomic, assign) AudioFileID audioFileID;
@property (nonatomic, assign) BOOL useAudioFile;
@property (nonatomic, assign) AudioStreamBasicDescription recordStreamBasicDescription;
@property (nonatomic, assign) AudioQueueRef audioQueue;

@property (nonatomic, retain) NSURL *audioFileURL;

@property (nonatomic, assign) UInt32 inNumPackets;

@property (nonatomic, assign) NSMutableData *audioData;

@property (nonatomic, assign) AudioStreamBasicDescription playStreamBasicDescription;

@property (nonatomic, assign) NSTimer *processTimer;
@property (nonatomic, assign) NSTimer *levelMeterTimer;

@property (nonatomic, assign) BOOL requestRecordingFinishProcessInStoppingQueue;
@property (nonatomic, assign) BOOL requestPlayingStartInStartingQueue;

- (void)runPrepareBlock:(BOOL)success;
- (void)createErrorDescription:(NSString *)errorString;

- (BOOL)openAudioFile:(NSURL *)audioFileURL
          audioFileID:(AudioFileID *)audioFile;
- (NSString *)getFileFormatType:(AudioFileID)audioFile;

- (void)setupAudioFormat:(AudioStreamBasicDescription *)asbd
           audioFormatID:(UInt32)audioFormat;
- (int)calculateBufferSize;
- (void)calculateBufferSize:(AudioFileID)audioFile
  audioStreamBasicDescription:(AudioStreamBasicDescription)asbd
                     duration:(Float64)duration
             byteBufferSize:(UInt32 *)byteBufferSize
           numPacketsToRead:(UInt32 *)numPacketsToRead;
;
- (void)setupAudioBuffers;

- (void)addMagicCookieToFile:(NSURL *)fileURL
                 audioFileID:(AudioFileID)audioFileID;
- (NSData *)getMagicCookie;

- (void)addMagicCookieFromFileToQueue:(AudioFileID)audioFileID
                           audioQueue:(AudioQueueRef)queue;

- (void)startTimer;
- (void)stopTimer;
- (void)pauseTimer;

- (void)pushTimeToDelegate:(id)sender;
- (void)pushLevelMeterToDelegate:(id)sender;

@end

@implementation AudioManager

static AudioManager *sharedManager = nil;

+ (AudioManager *)sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[AudioManager alloc] init];
    });
    return sharedManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.audioInputAvailable = NO;
        self.audioData = [[NSMutableData alloc] init];
        
        self.audioFileType = kAudioFileM4AType;
        self.audioEncodingFormat = kAudioFormatMPEG4AAC;
    }
    return self;
}

+ (BOOL)IsAvailableAudioFileTypeAndFormat:(AudioFileTypeAndFormatID)fileTypeAndFormatID
{
    /**
     *  녹음 가능한 파일 타입리스트의 사이즈를 요청
     */
    UInt32 propSize;
    OSStatus err = AudioFileGetGlobalInfoSize(kAudioFileGlobalInfo_WritableTypes,
                                              0,
                                              NULL,
                                              &propSize);
    if (noErr != err) {
        NSLog(@"%@", UInt32ToString(err));
        return NO;
    }
    
    UInt32 fileTypesCount = propSize / sizeof(UInt32);
    
    UInt32 fileTypes[fileTypesCount];
    
    /**
     *  녹음 가능 파일 타입 리스트를 요청
     */
    err = AudioFileGetGlobalInfo(kAudioFileGlobalInfo_WritableTypes,
                                 0,
                                 NULL,
                                 &propSize,
                                 &fileTypes);
    if (noErr != err) {
        NSLog(@"Couldn't get audio formats : %@", UInt32ToString(err));
        return NO;
    }
    
    /**
     *  해당 하는 파일 타입으로 인코딩 할 수 있는 오디오 인코딩 포맷
     */
    for (int i=0; i < fileTypesCount; i++) {
        UInt32 audioFileType = fileTypes[i];
        UInt32 formatIDsSize;
        AudioFileGetGlobalInfoSize(kAudioFileGlobalInfo_AvailableFormatIDs,
                                   sizeof(UInt32),
                                   &audioFileType,
                                   &formatIDsSize);
        
        UInt32 formatIDsCount = formatIDsSize / sizeof(UInt32);
        UInt32 formatIDs[formatIDsCount];
        AudioFileGetGlobalInfo(kAudioFileGlobalInfo_AvailableFormatIDs,
                               sizeof(UInt32),
                               &audioFileType,
                               &formatIDsSize,
                               &formatIDs);
        for (int j=0 ; j< formatIDsCount; j++) {
            if (fileTypeAndFormatID.mFileType == audioFileType && fileTypeAndFormatID.mFormatID == formatIDs[j]) {
                return YES;
            }
        }
    }
    
    NSLog(@"Couldn't use type:%@ format:%@", UInt32ToString(fileTypeAndFormatID.mFileType), UInt32ToString(fileTypeAndFormatID.mFormatID));
    return NO;
}

+ (NSString *)AudioFileExtension:(AudioFileTypeID)audioFileType
{
    UInt32 propSize;
    OSStatus status = AudioFileGetGlobalInfoSize(kAudioFileGlobalInfo_ExtensionsForType,
                               sizeof(audioFileType),
                               &audioFileType,
                               &propSize);
    if (noErr != status) {
        NSLog(@"Couldn't get audio file extensions size for %@", UInt32ToString(audioFileType));
        return nil;
    }
    
    CFArrayRef extensionArray = nil;
    
    status = AudioFileGetGlobalInfo(kAudioFileGlobalInfo_ExtensionsForType,
                                    sizeof(audioFileType),
                                    &audioFileType,
                                    &propSize,
                                    &extensionArray);
    
    if (noErr != status || extensionArray == nil || 0 == CFArrayGetCount(extensionArray)) {
        NSLog(@"Couldn't get audio file extension list for %@", UInt32ToString(audioFileType));
        return nil;
    }
    
    return [((NSArray *)extensionArray) firstObject];
}

+ (void)ConvertProcessTime:(NSTimeInterval)processTime
                   seconds:(NSInteger *)seconds
                   minutes:(NSInteger *)minutes
                     hours:(NSInteger *)hours
{
    if (hours) {
        NSInteger h = floor(processTime / 60. / 60.);
        processTime -= h * 60. * 60.;
        *hours = h;
    }
    if (minutes) {
        NSInteger m = floor(processTime / 60.);
        processTime -= m * 60.;
        *minutes = m;
    }
    if (seconds) {
        NSInteger s = floor(processTime);
        *seconds = s;
    }
}

+ (AudioBufferList *)AMAllocateBufferList:(AudioStreamBasicDescription)asbd frameCount:(int)frameCount
{
    int numberOfBuffers = asbd.mFormatFlags & kAudioFormatFlagIsNonInterleaved ? asbd.mChannelsPerFrame : 1;
    int channelsPerBuffer = asbd.mFormatFlags & kAudioFormatFlagIsNonInterleaved ? 1 : asbd.mChannelsPerFrame;
    int bytesPerBuffer = asbd.mBytesPerFrame * frameCount;
    
    AudioBufferList *audio = malloc(sizeof(AudioBufferList) + (numberOfBuffers-1)*sizeof(AudioBuffer));
    if ( !audio ) {
        return NULL;
    }
    audio->mNumberBuffers = numberOfBuffers;
    for ( int i=0; i<numberOfBuffers; i++ ) {
        if ( bytesPerBuffer > 0 ) {
            audio->mBuffers[i].mData = malloc(bytesPerBuffer);
            if ( !audio->mBuffers[i].mData ) {
                for ( int j=0; j<i; j++ ) free(audio->mBuffers[j].mData);
                free(audio);
                return NULL;
            }
        } else {
            audio->mBuffers[i].mData = NULL;
        }
        audio->mBuffers[i].mDataByteSize = bytesPerBuffer;
        audio->mBuffers[i].mNumberChannels = channelsPerBuffer;
    }
    return audio;
}

- (BOOL)openAudioFile:(NSURL *)audioFileURL
          audioFileID:(AudioFileID *)audioFile
{
    OSStatus err = AudioFileOpenURL((CFURLRef)audioFileURL,
                                    kAudioFileReadPermission,
                                    0,
                                    audioFile);
    if (noErr == err) {
        return YES;
    }
    return NO;
}

- (NSString *)getFileFormatType:(AudioFileID)audioFile
{
    UInt32 propSize;
    OSStatus err = AudioFileGetPropertyInfo(audioFile,
                                            kAudioFilePropertyFileFormat,
                                            &propSize,
                                            NULL);
    UInt32 fileFormat;
    err = AudioFileGetProperty(audioFile,
                               kAudioFilePropertyFileFormat,
                               &propSize,
                               &fileFormat);
    if (noErr == err) {
        NSString *fileTypeString = UInt32ToString(fileFormat);
        if (4 == fileTypeString.length) {
            return [fileTypeString substringToIndex:3];
        }
    }
    else
    {
        NSAssert(NO, @"%@", UInt32ToString(err));
    }
    return @"";
}

- (void)CheckError:(OSStatus)err
    errDescription:(NSString *)description
            succeed:(void (^)(BOOL success))succeed
{
    if (noErr == err) {
        if (succeed) {
            succeed(YES);
            Block_release(succeed);
            succeed = nil;
        }
        return;
    }
    if (succeed) {
        succeed(NO);
        Block_release(succeed);
        succeed = nil;
    }
    NSString *errorString = UInt32ToString(err);
    NSLog(@"ERROR -> %@ : %@", description, errorString);
    NSAssert(noErr == err, @"%@:%@", description, errorString);
    [self createErrorDescription:[NSString stringWithFormat:@"%@:%@", description, errorString]];
}
void MyInterruptionListener ( void  *inClientData,
                             UInt32 inInterruptionState )
{
    AudioManager *recorder = (AudioManager *)inClientData;
    switch (inInterruptionState) {
        case kAudioSessionBeginInterruption:
        {
            NSLog(@"Audio Recording interrupted");
        }
            break;
        case kAudioSessionEndInterruption:
        {
            [recorder CheckError:AudioQueueStart(recorder.audioQueue,
                                                 NULL)
                  errDescription:@"Couldn't audio queue restart failed"
                          succeed:nil];
        }
            break;
        default:
            break;
    }
}

/**
 *  세션 프로퍼티 리스너
 *
 *  @param inClientData AudioManager
 *  @param propID       세션 ID
 *  @param inDataSize   프로퍼티 사이즈
 *  @param inData       프로퍼티 데이터
 */
void PropertyListener(void *inClientData,
                      AudioSessionPropertyID propID,
                      UInt32 inDataSize,
                      const void *inData)
{
    AudioManager *manager = (AudioManager *)inClientData;
    switch (propID) {
        case kAudioSessionProperty_AudioRouteChange:
        {
            NSLog(@"Audio Route Changed");
            if (inDataSize == sizeof(CFStringRef)) {
                NSDictionary *routeInfo = (NSDictionary *)inData;
                if (routeInfo) {
                    NSLog(@"Input route code -> %@", routeInfo[@"OutputDeviceDidChange_NewRoute"]);
                }
            }
        }
            break;
        case kAudioSessionProperty_AudioInputAvailable:
        {
            // UInt32 데이터인지 체크
            if (sizeof(UInt32) == inDataSize) {
                UInt32 inputAvailable = *(UInt32 *)inData;
                // 오디오 입력이 가능한 경우 0 이상의 값이 들어옴(예: 1)
                if (0 < inputAvailable) {
                    manager.audioInputAvailable = YES;
                    /**
                     *  prepareBlock이 있을 경우 작업 진행
                     */
                    [manager runPrepareBlock:YES];
                }
                else
                {
                    manager.audioInputAvailable = NO;
                    [manager runPrepareBlock:NO];
                }
            }
        }
            break;
        default:
            break;
    }
}

/**
 *  오디오 큐 프로퍼티 리스너
 *
 *  @param inUserData AudioManager
 *  @param inAQ       오디오 큐
 *  @param inID       프로퍼티 ID
 *
 */
void MyAudioQueuePropertyListener (
                                   void                  *inUserData,
                                   AudioQueueRef         inAQ,
                                   AudioQueuePropertyID  inID
                                   )
{
    AudioManager *am = (AudioManager *)inUserData;
    switch (inID) {
        case kAudioQueueProperty_IsRunning:
        {
            UInt32 isRunning;
            UInt32 propSize = sizeof(isRunning);
            AudioQueueGetProperty(inAQ,
                                  kAudioQueueProperty_IsRunning,
                                  &isRunning,
                                  &propSize);
            if (0 < isRunning) {
                if (am.isRecording) {
                    [am.recordDelegate audioRecordStart:am];
                }
                if (am.isPlaying) {
                    [am.playerDelegate audioPlayStart:am];
                }
                [am startTimer];
            }
            else
            {
                [am stopTimer];
                if (am.requestRecordingFinishProcessInStoppingQueue && am.recordDelegate) {
                    am.requestRecordingFinishProcessInStoppingQueue = NO;
                    [am finishRecordingProcess];
                    [am.recordDelegate audioRecordFinished:am
                                                 audioData:nil];
                    
                    if (am.requestPlayingStartInStartingQueue) {
                        am.requestPlayingStartInStartingQueue = NO;
                        [am startAudioPlayInQueue];
                    }
                }
                if (am.isPlaying && am.playerDelegate) {
                    am->_playing = NO;
                    [am.playerDelegate audioPlayFinished:am];
                }
            }
        }
            break;
            break;
        default:
            break;
    }
}

void MyAudioQueueInputCallback (
                                void                                *inUserData,
                                AudioQueueRef                       inAQ,
                                AudioQueueBufferRef                 inBuffer,
                                const AudioTimeStamp                *inStartTime,
                                UInt32                              inNumberPacketDescriptions,
                                const AudioStreamPacketDescription  *inPacketDescs
                                )
{
    AudioManager *manager = (AudioManager *)inUserData;
    if (0 < inNumberPacketDescriptions) {
//        SInt16 swapedBytes[inBuffer->mAudioDataByteSize];
//        for (int i=0; i<inBuffer->mAudioDataByteSize; i++) {
//            swapedBytes[i] = CFSwapInt16HostToBig(((SInt16 *)inBuffer->mAudioData)[i]);
//        }
//        [manager.audioData appendBytes:swapedBytes
//                                length:inBuffer->mAudioDataByteSize];
        if (manager.useAudioFile) {
            [manager CheckError:AudioFileWritePackets(manager.audioFileID,
                                                      FALSE,
                                                      inBuffer->mAudioDataByteSize,
                                                      inPacketDescs,
                                                      manager.inNumPackets,
                                                      &inNumberPacketDescriptions,
                                                      inBuffer->mAudioData)
                 errDescription:@"AudioFileWritePackets error"
                         succeed:nil];
            manager.inNumPackets += inNumberPacketDescriptions;
        }
    }
    
    if (manager.isRecording) {
        [manager CheckError:AudioQueueEnqueueBuffer(inAQ,
                                                    inBuffer,
                                                    0,
                                                    NULL)
             errDescription:@"AudioQueueEnqueueBuffer on input callback failed"
                     succeed:^(BOOL success) {
                         assert(success);
                     }];
    }

}

- (void)createErrorDescription:(NSString *)errorString
{
    NSError *error = [NSError errorWithDomain:@"AudioManager"
                                         code:0
                                     userInfo:@{NSLocalizedFailureReasonErrorKey : errorString}];
    if (self.isRecording) {
        [self stopRecord];
        if (self.recordDelegate) {
            [self.recordDelegate audioRecordFailed:self
                                             error:error];
        }
    }
    
    if (self.isPlaying) {
        [self stopPlay];
        if (self.playerDelegate) {
            [self.playerDelegate audioPlayFailed:self
                                           error:error];
        }
    }
}

- (void)runPrepareBlock:(BOOL)success
{
    if (_prepareBlock) {
        _prepareBlock(success);
        Block_release(_prepareBlock);
        _prepareBlock = nil;
    }
}

- (void)setupAudioFormat:(AudioStreamBasicDescription *)asbd
           audioFormatID:(UInt32)audioFormat
{
    memset(asbd, 0, sizeof(AudioStreamBasicDescription));
    
    asbd->mFormatID = audioFormat;
    
    UInt32 sampleRateSize = sizeof(asbd->mSampleRate);
    [self CheckError:AudioSessionGetProperty(kAudioSessionProperty_CurrentHardwareSampleRate,
                                       &sampleRateSize,
                                       &asbd->mSampleRate)
     errDescription:@"Couldn't get current hardware sample rate"
     succeed:nil];
    
    UInt32 numberChannlesSize = sizeof(asbd->mChannelsPerFrame);
    [self CheckError:AudioSessionGetProperty(kAudioSessionProperty_CurrentHardwareInputNumberChannels,
                                       &numberChannlesSize,
                                       &asbd->mChannelsPerFrame)
     errDescription:@"Couldn't get current hardware input number channels"
     succeed:nil];
    
    switch (asbd->mFormatID) {
        case kAudioFormatLinearPCM:
        {
            asbd->mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
            asbd->mBitsPerChannel = 16;
            asbd->mBytesPerFrame = asbd->mBytesPerPacket = (asbd->mBitsPerChannel / 8) * asbd->mChannelsPerFrame;
            asbd->mFramesPerPacket = 1;
        }
            break;
        default:
            break;
    }
}

- (int)calculateBufferSize
{
    int packets, frames, bytes;
    
    frames = kBufferDuration * (int)ceil(_recordStreamBasicDescription.mSampleRate * kBufferDuration);
    
    if (0 < _recordStreamBasicDescription.mBytesPerFrame) {
        bytes = _recordStreamBasicDescription.mBytesPerFrame * frames;
    }
    else
    {
        UInt32 mBytesPerPacket = 0;
        if (0 < _recordStreamBasicDescription.mBytesPerPacket) {
            mBytesPerPacket = _recordStreamBasicDescription.mBytesPerPacket;
        }
        else
        {
            UInt32 propSize = sizeof(mBytesPerPacket);
            [self CheckError:AudioQueueGetProperty(_audioQueue,
                                             kAudioQueueProperty_MaximumOutputPacketSize,
                                             &mBytesPerPacket,
                                             &propSize)
             errDescription:@"Couldn't get Maximum Output Packet Size"
             succeed:nil];
        }
        if (0 < _recordStreamBasicDescription.mFramesPerPacket) {
            packets = frames / _recordStreamBasicDescription.mFramesPerPacket;
        }
        else
        {
            packets = frames;
        }
        
        if (0 == packets) {
            packets = 1;
        }
        bytes = packets * mBytesPerPacket;
    }
    
    return bytes;
}

- (void)calculateBufferSize:(AudioFileID)audioFile
audioStreamBasicDescription:(AudioStreamBasicDescription)asbd
                   duration:(Float64)duration
             byteBufferSize:(UInt32 *)byteBufferSize
           numPacketsToRead:(UInt32 *)numPacketsToRead
{
    UInt32 maxPacketSize;
    UInt32 propSize = sizeof(maxPacketSize);
    [self CheckError:AudioFileGetProperty(audioFile,
                                          kAudioFilePropertyPacketSizeUpperBound,
                                          &propSize,
                                          &maxPacketSize)
      errDescription:@"Couldn't get Max Packet Size from file failed"
              succeed:nil];
    
    static const int maxBufferSize = 0x10000;
    static const int minBufferSize = 0x4000;
    
    if (asbd.mFramesPerPacket) {
        Float64 numPacketsForTime = asbd.mSampleRate / asbd.mFramesPerPacket * duration;
        *byteBufferSize = numPacketsForTime * maxPacketSize;
    }
    else
    {
        *byteBufferSize = maxBufferSize > maxPacketSize ? maxBufferSize : maxPacketSize;
    }
    
    if (*byteBufferSize > maxBufferSize && *byteBufferSize > maxPacketSize) {
        *byteBufferSize = maxBufferSize;
    }
    else if (*byteBufferSize < minBufferSize)
    {
        *byteBufferSize = minBufferSize;
    }
    *numPacketsToRead = *byteBufferSize / maxPacketSize;
}

- (void)setupAudioBuffers
{
    UInt32 bufferSize = [self calculateBufferSize];
    for (int i = 0; i < kNumberBuffers; i++) {
        [self CheckError:AudioQueueAllocateBuffer(_audioQueue,
                                            bufferSize,
                                            &buffers[i])
                         errDescription: @"AudioQueueAllocateBuffer failed"
                  succeed:nil];
        [self CheckError:AudioQueueEnqueueBuffer(_audioQueue,
                                           buffers[i],
                                           0,
                                           NULL)
         errDescription:@"AudioQueueEnqueuBuffer failed"
         succeed:nil];
    }
}

- (void)addMagicCookieToFile:(NSURL *)fileURL
                 audioFileID:(AudioFileID)audioFileID
{
    if (fileURL) {
        OSStatus err = noErr;
        UInt32 magicCookieSize;
        err = AudioQueueGetPropertySize(_audioQueue,
                                                   kAudioQueueProperty_MagicCookie,
                                                   &magicCookieSize);
        if (noErr == err && 0 < magicCookieSize) {
            Byte *magicCookie = (Byte *)malloc(magicCookieSize);
            [self CheckError:AudioQueueGetProperty(_audioQueue,
                                                   kAudioQueueProperty_MagicCookie,
                                                   magicCookie,
                                                   &magicCookieSize)
              errDescription:@"Couldn't get magic cookie"
                      succeed:nil];
            [self CheckError:AudioFileSetProperty(audioFileID,
                                                  kAudioFilePropertyMagicCookieData,
                                                  magicCookieSize,
                                                  magicCookie)
              errDescription:@"Couldn't set magic cookie to file"
                      succeed:nil];
            free(magicCookie);
        }
    }
}

- (NSData *)getMagicCookie
{
    NSData *data = nil;
    
    OSStatus err = noErr;
    UInt32 magicCookieSize;
    err = AudioSessionGetPropertySize(kAudioConverterCompressionMagicCookie, &magicCookieSize);
    if (noErr == err && 0 < magicCookieSize) {
        Byte *magicCookie = (Byte *)malloc(magicCookieSize);
        [self CheckError:AudioSessionGetProperty(kAudioConverterCompressionMagicCookie,
                                                 &magicCookieSize,
                                                 magicCookie)
          errDescription:@"Couldn't get magic cookie"
                  succeed:nil];
        data = [NSData dataWithBytes:magicCookie
                              length:magicCookieSize];
        free(magicCookie);
    }
    
    return data;
}

- (void)addMagicCookieFromFileToQueue:(AudioFileID)audioFileID
                           audioQueue:(AudioQueueRef)queue
{
    UInt32 propertySize;
    OSStatus result = AudioFileGetPropertyInfo(audioFileID,
                                               kAudioFilePropertyMagicCookieData,
                                               &propertySize,
                                               NULL);
    if (noErr == result) {
        Byte *magicCookieData = (Byte *)malloc(sizeof(Byte) * propertySize);
        [self CheckError:AudioFileGetProperty(audioFileID,
                                              kAudioFilePropertyMagicCookieData,
                                              &propertySize,
                                              magicCookieData)
          errDescription:@"Couldn't get magic cookie data from file failed"
                  succeed:nil];
        
        [self CheckError:AudioQueueSetProperty(queue,
                                               kAudioQueueProperty_MagicCookie,
                                               magicCookieData,
                                               propertySize)
          errDescription:@"Couldn't set magic cookie to queue failed"
                  succeed:nil];
        free(magicCookieData);
    }
}

static BOOL AudioSessionInitialized = NO;

- (void)prepareAudioSession:(void (^)(BOOL success))prepare
{
    self.prepareBlock = prepare;
    
    if (NO == AudioSessionInitialized) {
        AudioSessionInitialized = YES;
        [self CheckError:AudioSessionInitialize(NULL,
                                                kCFRunLoopDefaultMode,
                                                MyInterruptionListener,
                                                self)
          errDescription:@"AudioSessionInitialize failed"
                 succeed:nil];
    }
    
    UInt32 category = kAudioSessionCategory_PlayAndRecord;
    [self CheckError:AudioSessionSetProperty(kAudioSessionProperty_AudioCategory,
                                             sizeof(category),
                                             &category)
      errDescription:@"Couldn't set audio session cateogry"
              succeed:nil];
    
    [self CheckError:AudioSessionAddPropertyListener(kAudioSessionProperty_AudioRouteChange,
                                                     PropertyListener,
                                                     self)
      errDescription:@"Audio route change property listener set failed"
              succeed:nil];
    
    UInt32 inputAvailable = 0;
    UInt32 inputAvailableSize = sizeof(UInt32);
    [self CheckError:AudioSessionGetProperty(kAudioSessionProperty_AudioInputAvailable,
                                             &inputAvailableSize,
                                             &inputAvailable)
      errDescription:@"Audio Input Available check failed"
              succeed:nil];
    
    if (0 < inputAvailable) {
        
        CFStringRef inputRouteCode = nil;
        UInt32 inputRouteCodeSize = sizeof(CFStringRef);
        [self CheckError:AudioSessionGetProperty(kAudioSessionProperty_AudioRoute,
                                                 &inputRouteCodeSize,
                                                 &inputRouteCode)
          errDescription:@"Couldn't get input route code"
                  succeed:nil];
        
        if (inputRouteCode) {
            NSLog(@"input route code -> %@", (NSString *)inputRouteCode);
        }
        
        self.audioInputAvailable = YES;
    }
    else
    {
        self.audioInputAvailable = NO;
    }
    
    [self CheckError:AudioSessionAddPropertyListener(kAudioSessionProperty_AudioInputAvailable,
                                                     PropertyListener,
                                                     self)
      errDescription:@"Couldn't add audio input available listener"
              succeed:nil];
    
    [self CheckError:AudioSessionSetActive(TRUE)
      errDescription:@"Couldn't audio session set active"
              succeed:nil];
    
    if (self.audioInputAvailable) {
        [self runPrepareBlock:YES];
    }
}

- (void)setupAudioFile
{
    // 파일 만들기
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                               NSUserDomainMask,
                                                               YES) firstObject] stringByAppendingPathComponent:kAudioFileName];
    [[NSFileManager defaultManager] removeItemAtPath:filePath
                                               error:nil];
    NSString *extension = [AudioManager AudioFileExtension:self.audioFileType];
    filePath = [filePath stringByAppendingPathExtension:extension];
    self.audioFileURL = [NSURL fileURLWithPath:filePath];
}

- (void)startRecord:(BOOL)paused
{
    
    if (self.isPlaying) {
        [self stopPlay];
        return;
    }
    
    _recording = YES;
    _requestRecordingFinishProcessInStoppingQueue = NO;
    _paused = NO;
    if (paused) {
        [self CheckError:AudioQueueStart(_audioQueue,
                                         NULL)
          errDescription:@"AudioQueueRestart failed"
                  succeed:nil];
    }
    else
    {
        [self.audioData setData:nil];
        [self.audioData setLength:0];
        
        [self setupAudioFormat:&_recordStreamBasicDescription
                 audioFormatID:self.audioEncodingFormat];
        
        // 큐 만들기
        [self CheckError:AudioQueueNewInput(&_recordStreamBasicDescription,
                                      MyAudioQueueInputCallback,
                                      self,
                                      NULL,
                                      kCFRunLoopDefaultMode,
                                      0,
                                      &_audioQueue)
         errDescription:@"AudioQueueNewInput failed"
         succeed:^(BOOL success) {
             if (NO == success) {
                 assert(NO);
             }
         }];
        
        /**
         *  큐 체크 프로퍼티 리스너 설정
         */
        {
            [self CheckError:AudioQueueAddPropertyListener(_audioQueue,
                                                           kAudioQueueProperty_IsRunning,
                                                           MyAudioQueuePropertyListener,
                                                           self)
              errDescription:@"AudioQueueAddPropertyListener failed"
                      succeed:nil];
        }
        
        UInt32 propertySize;
        [self CheckError:AudioQueueGetPropertySize(_audioQueue,
                                                   kAudioQueueProperty_StreamDescription,
                                                   &propertySize)
          errDescription:@"Couldn't get audio format info"
                  succeed:nil];
        
        [self CheckError:AudioQueueGetProperty(_audioQueue,
                                               kAudioQueueProperty_StreamDescription,
                                               &_recordStreamBasicDescription,
                                               &propertySize)
          errDescription:@"Couldn't get audio format"
                  succeed:nil];
        {
            [self setupAudioFile];
            BOOL makeAudioFile = YES;
            if (makeAudioFile) {
                self.useAudioFile = YES;
                UInt32 permission = kAudioFileFlags_EraseFile;
                [self CheckError:AudioFileCreateWithURL((CFURLRef)self.audioFileURL,
                                                        self.audioFileType,
                                                        &_recordStreamBasicDescription,
                                                        permission,
                                                        &_audioFileID)
                  errDescription:@"AudioFileOpenURL failed"
                          succeed:nil];
                [self addMagicCookieToFile:self.audioFileURL
                               audioFileID:self.audioFileID];
            }
            
        }
        
        [self setupAudioBuffers];
        
        [self CheckError:AudioQueueStart(_audioQueue,
                                         NULL)
          errDescription:@"AudioQueueStart failed"
                  succeed:^(BOOL success) {
                      if (NO == success) {
                          _recording = NO;
                      }
                  }];
    }
}

- (void)finishRecordingProcess
{
    dispatch_async(dispatch_get_main_queue(), ^{
        for (int i=0 ; i < kNumberBuffers ; i++) {
            [self CheckError:AudioQueueFreeBuffer(_audioQueue,
                                                  buffers[i])
              errDescription:@"AudioQueueFreeBuffer failed"
                      succeed:nil];
        }
        
        if (self.useAudioFile) {
            [self addMagicCookieToFile:self.audioFileURL
                           audioFileID:self.audioFileID];
        }
        
        [self CheckError:AudioQueueDispose(_audioQueue, TRUE)
          errDescription:@"AudioQueueDispose failed"
                  succeed:nil];
        
        if (self.useAudioFile) {
            [self CheckError:AudioFileClose(_audioFileID)
              errDescription:@"AudioFileClose failed"
                      succeed:nil];
        }
        
        self.inNumPackets = 0;
    });
}

- (void)stopRecord
{
    _paused = NO;
    _recording = NO;
    _requestRecordingFinishProcessInStoppingQueue = YES;
    
    [self CheckError:AudioQueueStop(_audioQueue,
                                    TRUE)
      errDescription:@"AudioQueueStop failed"
              succeed:nil];
}

- (void)pauseRecord
{
    _paused = YES;
    [self CheckError:AudioQueuePause(_audioQueue)
      errDescription:@"AudioQueuePause failed"
              succeed:nil];
}

- (void)saveDataToAudioFile:(NSURL *)fileURL
                       data:(NSData *)data
audioStreamBasicDescription:(AudioStreamBasicDescription)asbd
{
    AudioFileID audioFile;
    [self CheckError:AudioFileCreateWithURL((CFURLRef)fileURL,
                                            self.audioFileType,
                                            &asbd,
                                            kAudioFileFlags_EraseFile,
                                            &audioFile)
      errDescription:@"AudioFileCreateWithURL failed"
              succeed:nil];
    [self addMagicCookieToFile:fileURL
                   audioFileID:audioFile];
    NSError *err = nil;
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingURL:fileURL
                                                                error:&err];
    [fileHandle seekToEndOfFile];
    [fileHandle writeData:data];
    [fileHandle closeFile];
    
    AudioFileClose(audioFile);
    
    if (err) {
        NSLog(@"%@", err.userInfo);
    }
}

#pragma mark - Player

void MyAQOutputCallback( void *inClientData,
                        AudioQueueRef queue,
                        AudioQueueBufferRef buffer)
{
    AudioManager *manager = (AudioManager *)inClientData;
    if (manager->isDone) {
        return;
    }
    
    UInt32 numBytes;
    UInt32 numPackets = manager->playNumPacketsToRead;
    
    [manager CheckError:AudioFileReadPackets(manager.audioFileID,
                                             FALSE,
                                             &numBytes,
                                             manager->playAudioPacketDescriptions,
                                             manager->playPacketPosition,
                                             &numPackets,
                                             buffer->mAudioData)
         errDescription:@"AudioFileReadPackets failed"
                 succeed:nil];
    if (numPackets > 0) {
        buffer->mAudioDataByteSize = numBytes;
        
        AudioQueueEnqueueBuffer(queue,
                                buffer,
                                manager->playAudioPacketDescriptions ? numPackets : 0,
                                manager->playAudioPacketDescriptions);
        manager->playPacketPosition += numPackets;
    }
    else
    {
        [manager CheckError:AudioQueueStop(queue,
                                           false)
             errDescription:@"AudioQueueStop failed"
                     succeed:nil];
        manager->isDone = true;
    }
}

/**
 *  오디오 큐가 실제로 시작한 후에 호출하는 플레이 큐 설정
 */
- (void)startAudioPlayInQueue
{
    dispatch_async(dispatch_get_main_queue(), ^{
        BOOL success = [self openAudioFile:self.audioFileURL
                               audioFileID:&_audioFileID];
        if (NO == success) {
            NSAssert(NO, @"Couldn't open play file %@", self.audioFileURL);
            [self createErrorDescription:@"OpenAudioFile failed"];
        }
        memset(&_playStreamBasicDescription, 0, sizeof(AudioStreamBasicDescription));
        
        UInt32 propSize = sizeof(AudioStreamBasicDescription);
        [self CheckError:AudioFileGetProperty(_audioFileID,
                                              kAudioFilePropertyDataFormat,
                                              &propSize,
                                              &_playStreamBasicDescription)
          errDescription:@"AudioFileGet Stream Basic Description failed"
                  succeed:nil];
        
        [self CheckError:AudioQueueNewOutput(&_playStreamBasicDescription,
                                             MyAQOutputCallback,
                                             self,
                                             NULL,
                                             kCFRunLoopDefaultMode,
                                             0,
                                             &_audioQueue)
          errDescription:@"AudioQueueNewOutput failed"
                  succeed:nil];
        
        /**
         *  사운드 플레이 프로퍼티 리스너 추가
         */
        {
            [self CheckError:AudioQueueAddPropertyListener(_audioQueue,
                                                           kAudioQueueProperty_IsRunning,
                                                           MyAudioQueuePropertyListener,
                                                           self)
              errDescription:@"AudioQueueAddPropertyListner failed"
                      succeed:nil];
            
            [self CheckError:AudioQueueAddPropertyListener(_audioQueue,
                                                           kAudioQueueProperty_CurrentLevelMeter,
                                                           MyAudioQueuePropertyListener,
                                                           self)
              errDescription:@"AudioQueueAddPropertyListner LevelMeter failed"
                      succeed:nil];
        }
        
        UInt32 byteBufferSize;
        [self calculateBufferSize:_audioFileID
      audioStreamBasicDescription:_playStreamBasicDescription
                         duration:kBufferDuration
                   byteBufferSize:&byteBufferSize
                 numPacketsToRead:&playNumPacketsToRead];
        
        BOOL isFormatVBR = _playStreamBasicDescription.mBytesPerPacket == 0 || _playStreamBasicDescription.mFramesPerPacket == 0;
        if (isFormatVBR) {
            playAudioPacketDescriptions = (AudioStreamPacketDescription *)malloc(sizeof(AudioStreamPacketDescription) * playNumPacketsToRead);
        }
        else
        {
            playAudioPacketDescriptions = NULL;
        }
        
        [self addMagicCookieFromFileToQueue:_audioFileID
                                 audioQueue:_audioQueue];
        
        isDone = NO;
        playPacketPosition = 0;
        
        memset(buffers, 0, sizeof(AudioQueueBufferRef) * kNumberBuffers);
        for (int i=0 ; i < kNumberBuffers ; i++) {
            [self CheckError:AudioQueueAllocateBuffer(_audioQueue,
                                                      byteBufferSize,
                                                      &buffers[i])
              errDescription:@"AudioQueueAllocateBuffer failed"
                      succeed:nil];
            MyAQOutputCallback(self, _audioQueue, buffers[i]);
            
            if (isDone) {
                break;
            }
        }
        
        Float32 volume = 1.0f;
        AudioQueueSetParameter(_audioQueue,
                               kAudioQueueParam_Volume,
                               volume);
        [self CheckError:AudioQueueStart(_audioQueue,
                                         NULL)
          errDescription:@"AudioQueueStart failed"
                  succeed:nil];
    });
}

- (void)startPlayWithFile:(NSURL *)fileURL
{
    _audioFileURL = fileURL;
    [self startPlay:NO];
}

- (void)startPlay:(BOOL)paused
{
    _playing = YES;
    _paused = NO;
    self.requestPlayingStartInStartingQueue = NO;
    
    if (self.isRecording) {
        [self stopRecord];
        self.requestPlayingStartInStartingQueue = YES;
        return;
    }
    
    if (nil == self.audioFileURL) {
        [[self playerDelegate] audioPlayFailed:self
                                         error:[NSError errorWithDomain:NSCocoaErrorDomain
                                                                   code:-1
                                                               userInfo:@{NSLocalizedFailureReasonErrorKey : @"Audio File is null"}]];
        return;
    }
    
    if (paused) {
        [self CheckError:AudioQueueStart(_audioQueue,
                                         NULL)
          errDescription:@"AudioQueueRestart failed"
                  succeed:nil];
    }
    else
    {
        [self startAudioPlayInQueue];
    }
    
}

- (void)stopPlay
{
    isDone = NO;
    _playing = NO;
    _paused = NO;
    
    [self CheckError:AudioQueueStop(_audioQueue,
                                    TRUE)
      errDescription:@"AudioQueueStop failed"
              succeed:nil];
    
    for (int i=0 ; i < kNumberBuffers ; i++) {
        [self CheckError:AudioQueueFreeBuffer(_audioQueue,
                                              buffers[i])
          errDescription:@"AudioQueueFreeBuffer failed"
                  succeed:nil];
    }
    AudioQueueDispose(_audioQueue,
                      TRUE);
    AudioFileClose(_audioFileID);
    
    if (_playerDelegate) {
        [_playerDelegate audioPlayFinished:self];
    }
}

- (void)pausePlay
{
    _paused = YES;
    [self CheckError:AudioQueuePause(_audioQueue)
      errDescription:@"AudioQueuePause failed"
              succeed:nil];
}

#pragma mark - Timer

- (void)startTimer
{
    if (self.processTimer) {
        [self.processTimer invalidate];
        self.processTimer = nil;
    }
    self.processTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                         target:self
                                                       selector:@selector(pushTimeToDelegate:)
                                                       userInfo:[NSMutableDictionary dictionaryWithObjectsAndKeys:@(0.0), @"TIME", nil]
                                                        repeats:YES];
}

- (void)stopTimer
{
    [self.processTimer invalidate];
    self.processTimer = nil;
}

- (void)pauseTimer
{
    [self stopTimer];
}

- (void)pushTimeToDelegate:(id)sender
{
    NSTimer *timer = sender;
    NSMutableDictionary *timeDic = timer.userInfo;
    NSNumber *time = timeDic[@"TIME"];
    time = @(time.floatValue + timer.timeInterval);
    [timeDic setObject:time
                forKey:@"TIME"];
    if (self.isRecording) {
        [self.recordDelegate audioRecord:self
                             processTime:time.doubleValue];
    }
    if (self.isPlaying) {
        [self.playerDelegate audioPlay:self
                           processTime:time.doubleValue];
    }
}

- (void)pushLevelMeterToDelegate:(id)sender
{
    
}

- (Float32)currentLevelMeter
{
    Float32 volumeMeter;
    if (self.isRecording || self.isPlaying) {
        
    }
    return volumeMeter;
}

#pragma mark - Audio Pass Tru to mic

#pragma mark - Audio pass render callback

OSStatus MyAURenderCallback (
                             void                        *inRefCon,
                             AudioUnitRenderActionFlags  *ioActionFlags,
                             const AudioTimeStamp        *inTimeStamp,
                             UInt32                      inBusNumber,
                             UInt32                      inNumberFrames,
                             AudioBufferList             *ioData
                             )
{
    OSStatus status = noErr;
    AudioManager *am = inRefCon;
    
    status = AudioUnitRender(am->_remoteAudioUnit,
                    ioActionFlags,
                    inTimeStamp,
                    1,
                    inNumberFrames,
                    ioData);
    if (noErr != status) {
        NSLog(@"AudioUnitRender failed %@", UInt32ToString(status));
        return status;
    }
    
    if (am.passTruDelegate) {
        [am.passTruDelegate audioPassTru:am
                                  frames:inNumberFrames
                              bufferList:ioData];
    }
    
    return noErr;
}

#pragma mark - Request Audio Pass Tru to mic

- (void)requestPassTruToSpeakerFromMic
{
    
    AudioUnit remoteIOUnit;
    
    AudioComponentDescription acd;
    acd.componentType = kAudioUnitType_Output;
    acd.componentSubType = kAudioUnitSubType_RemoteIO;
    acd.componentManufacturer = kAudioUnitManufacturer_Apple;
    acd.componentFlags = 0;
    acd.componentFlagsMask = 0;
    
    AudioComponent comp = AudioComponentFindNext(NULL, &acd);
    
    [self CheckError:AudioComponentInstanceNew(comp,
                                            &remoteIOUnit)
      errDescription:@"AudioComponentFindNext failed"
             succeed:nil];
    
    UInt32 ioUnit = 1;
    [self CheckError:AudioUnitSetProperty(remoteIOUnit,
                                          kAudioOutputUnitProperty_EnableIO,
                                          kAudioUnitScope_Input,
                                          1,
                                          &ioUnit, sizeof(ioUnit))
      errDescription:@"Couldn't enable input remote io unit"
             succeed:nil];

    AURenderCallbackStruct renderCallbackStruct;
    renderCallbackStruct.inputProc = &MyAURenderCallback;
    renderCallbackStruct.inputProcRefCon = self;
    
    [self CheckError:AudioUnitSetProperty(remoteIOUnit,
                                          kAudioUnitProperty_SetRenderCallback,
                                          kAudioUnitScope_Input,
                                          0,
                                          &renderCallbackStruct,
                                          sizeof(renderCallbackStruct))
      errDescription:@"Couldn't add au render callback"
             succeed:nil];
    
    AudioStreamBasicDescription asbd;
//    [self setupAudioFormat:&asbd
//             audioFormatID:kAudioFormatLinearPCM];
//    asbd.mFormatFlags = kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked | kAudioFormatFlagIsFloat | kAudioFormatFlagIsNonInterleaved;
    
    memset(&asbd, 0, sizeof(AudioStreamBasicDescription));
    asbd.mFormatID = kAudioFormatLinearPCM;
    
    UInt32 sampleRateSize = sizeof(asbd.mSampleRate);
    [self CheckError:AudioSessionGetProperty(kAudioSessionProperty_CurrentHardwareSampleRate,
                                             &sampleRateSize,
                                             &asbd.mSampleRate)
      errDescription:@"Couldn't get current hardware sample rate"
             succeed:nil];
    
    UInt32 numberChannlesSize = sizeof(asbd.mChannelsPerFrame);
    [self CheckError:AudioSessionGetProperty(kAudioSessionProperty_CurrentHardwareInputNumberChannels,
                                             &numberChannlesSize,
                                             &asbd.mChannelsPerFrame)
      errDescription:@"Couldn't get current hardware input number channels"
             succeed:nil];
    
    switch (asbd.mFormatID) {
        case kAudioFormatLinearPCM:
        {
            asbd.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
            asbd.mBitsPerChannel = 16;
            asbd.mBytesPerFrame = asbd.mBytesPerPacket = (asbd.mBitsPerChannel / 8) * asbd.mChannelsPerFrame;
            asbd.mFramesPerPacket = 1;
        }
            break;
        default:
            break;
    }
    
    [self CheckError:AudioUnitSetProperty(remoteIOUnit,
                                          kAudioUnitProperty_StreamFormat,
                                          kAudioUnitScope_Input,
                                          0,
                                          &asbd,
                                          sizeof(asbd))
      errDescription:@"Couldn't set stream format to input au"
             succeed:nil];
    
    [self CheckError:AudioUnitSetProperty(remoteIOUnit,
                                          kAudioUnitProperty_StreamFormat,
                                          kAudioUnitScope_Output,
                                          1,
                                          &asbd,
                                          sizeof(asbd))
      errDescription:@"Couldn't set stream format to output au"
             succeed:nil];
    

    [self CheckError:AudioUnitInitialize(remoteIOUnit)
      errDescription:@"AudioUnitInitialize failed"
             succeed:nil];
    
    if (self.passTruDelegate) {
        [self.passTruDelegate audioPassTruStart:self
                                           asbd:asbd];
    }
    
    [self CheckError:AudioOutputUnitStart(remoteIOUnit)
      errDescription:@"AudioOutputUnitStart failed"
             succeed:^(BOOL success) {
                 _inPassTru = success;
                 if (success) {
                     _remoteAudioUnit = remoteIOUnit;
                 }
                 else
                 {
                     _remoteAudioUnit = NULL;
                     assert(NO);
                 }
             }];
}

- (void)stopPassTruToSpeakerFromMic
{
    AudioOutputUnitStop(_remoteAudioUnit);
    AudioUnitUninitialize(_remoteAudioUnit);
    _remoteAudioUnit = NULL;
    _inPassTru = NO;
}


- (void)generateSampleSound
{
    
}

@end
